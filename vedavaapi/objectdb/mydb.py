from collections import OrderedDict

import six


# Base class to encapsulate a MYDB Table
class DBMSCollection:

    operations = None  # type: any

    def __init__(self, dbms_db, collection_name):
        self.dbms_db = dbms_db
        self.name = collection_name
        # subclass should override it along with calling super.

    def all(self):
        raise TypeError("No method definition available: DBMSCollection.all.")
        
    def count(self, filter_doc, **kwargs):
        raise TypeError("No method definition available: DBMSCollection.count.")
        
    def drop(self):
        raise TypeError("No method definition available: DBMSCollection.reset.")
        
    def find_one(self, query, projection=None):
        raise TypeError("No method definition available: DBMSCollection.find_one.")

    def find(self, query, projection=None, cursor=False):
        raise TypeError("No method definition available: DBMSCollection.find")
        
    def insert_one(self, item):
        raise TypeError("No method definition available: DBMSCollection.insert_one.")

    def insert_many(self, items):
        raise TypeError("No method definition available: DBMSCollection.insert_many.")
        
    def update_one(self, query, mod_spec, upsert=False):
        raise TypeError("No method definition available: DBMSCollection.update_one.")

    def update_many(self, query, mod_spec, upsert=False):
        raise TypeError("No method definition available: DBMSCollection.update_many.")

    def delete_one(self, query):
        raise TypeError("No method definition available: DBMSCollection.delete_one.")

    def delete_many(self, query):
        raise TypeError("No method definition available: DBMSCollection.delete_many.")

    def bulk_write(self, requests, ordered=True, bypass_document_validation=False, session=None):
        raise TypeError("No method definition available: DBMSCollection.delete_many.")

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        raise TypeError("No method definition available: DBMSCollection.find_one_and_update.")

    def object_id(self, _id):
        raise TypeError("No method definition available: DBMSCollection.object_id.")

    def string_id(self, _id):
        pass

    def create_index(self, keys_dict, index_name):
        raise TypeError("No method definition available: DBMSCollection.create_index.")


class DBMSDatabase(object):

    def __init__(self, dbms_client, db_name):
        self.dbms_client = dbms_client
        self.name = db_name
        # should override it according to each dbms, after calling super

    def __getitem__(self, name):
        return self.get_collection(name)

    def list_collection_names(self):
        pass

    def get_collection(self, collection_name):
        pass

    def drop_collection(self, collection_name):
        pass


class DBMSClient(object):

    def __init__(self, host_uri, port=None):
        self.host_uri = host_uri
        self.port = port
        # should override according to each DBMS after calling super

    def list_database_names(self):
        pass

    def get_database(self, db_name):
        pass

    def drop_database(self, db_name):
        pass

    def close(self):
        pass

        
# MYDB collection with import/export functionality
class MyDbCollection:
    def __init__(self, dbms_collection, cache=False):
        self.dbms_collection = dbms_collection  # type: DBMSCollection
        self.name = self.dbms_collection.name
        self.cache = cache

    def get_descriptor(self):
        dbms_client = self.dbms_collection.dbms_db.dbms_client
        return {
            "dbms_client_cls_module": dbms_client.__class__.__module__ ,
            "dbms_client_cls": dbms_client.__class__.__name__,
            "client_params": {
                "host_uri": dbms_client.host_uri,
                "port": dbms_client.port
            },
            "db_params": {
                "name": self.dbms_collection.dbms_db.name
            },
            "colln_params": {
                "name": self.dbms_collection.name
            }
        }

    @classmethod
    def from_descriptor(cls, descriptor):
        import importlib
        module = importlib.import_module(descriptor['dbms_client_cls_module'])
        dbms_client_cls = getattr(module, descriptor['dbms_client_cls'])

        dbms_client = dbms_client_cls(descriptor['client_params']['host_uri'], descriptor['client_params']['port'])
        dbms_db = dbms_client.get_database(descriptor['db_params']['name'])
        dbms_colln = dbms_db.get_collection(descriptor['colln_params']['name'])

        return cls(dbms_colln, cache=descriptor.get('cache', False))


    def marshal_string_ids2object_ids(self, doc, _id_in_doc_path=False):
        if isinstance(doc, six.string_types):
            return self.dbms_collection.object_id(doc) if _id_in_doc_path else doc

        if isinstance(doc, list):
            return [self.marshal_string_ids2object_ids(item, _id_in_doc_path=_id_in_doc_path) for item in doc]

        if isinstance(doc, dict):
            doc_copy = doc.copy()
            for k, v in doc.items():
                doc_copy[k] = self.marshal_string_ids2object_ids(
                    v, _id_in_doc_path=_id_in_doc_path or (k == '_id')
                )
            return doc_copy
        else:
            return doc

    def marshal_object_id2string_id(self, doc):
        if doc is not None and "_id" in doc:
            doc["_id"] = self.dbms_collection.string_id(doc["_id"])
        return doc

    def query_doc_for_id(self, item_id):
        try:
            return {'_id': self.dbms_collection.object_id(item_id)}
        except Exception as e:
            print("Error: invalid object id ", e)
            return None

    def count(self, selector_doc, **kwargs):
        ops = OrderedDict()
        ops['count'] = []
        return self.find_and_do(selector_doc, ops, projection={"_id": 1})

    def get(self, doc_id, projection=None):
        query = self.query_doc_for_id(doc_id)
        if query is None:
            return None

        doc = self.dbms_collection.find_one(query, projection=projection)
        self.marshal_object_id2string_id(doc)
        return doc

    def find_one(self, query, projection=None):
        query = self.marshal_string_ids2object_ids(query)
        doc = self.dbms_collection.find_one(query, projection=projection)
        self.marshal_object_id2string_id(doc)
        return doc

    def _results_iterator(self, results_cursor, return_cursor=False, return_generator=False):
        if return_cursor:
            return results_cursor
        if return_generator:
            return self._results_generator(results_cursor)
        return list(self._results_generator(results_cursor))

    def _results_generator(self, results):
        for result in results:
            self.marshal_object_id2string_id(result)
            yield result

    def find(self, query=None, projection=None, return_cursor=False, return_generator=False):
        #  print('mydbcolln: {}: find'.format(self.name))
        query = self.marshal_string_ids2object_ids(query or {})
        results_cursor = self.dbms_collection.find(query, projection=projection)
        return self._results_iterator(results_cursor, return_cursor=return_cursor, return_generator=return_generator)

    def find_and_do(self, query, ops, projection=None, return_cursor=False, return_generator=False):
        #  print('mydbcolln: {}: find_and_do'.format(self.name))
        should_operate_on_cursor = True
        supported_ops = ['limit', 'skip', 'sort', 'count']
        for op in ops.keys():
            if op not in supported_ops:
                should_operate_on_cursor = False

        result_cursor = self.find(query=query, projection=projection, return_cursor=True)
        resultant = result_cursor

        from pymongo.cursor import Cursor  # TODO standardise

        if should_operate_on_cursor:
            for op, args in ops.items():
                if not isinstance(resultant, Cursor):
                    break
                op_method = resultant.__getattribute__(op)
                resultant = op_method(*args)

        if not isinstance(resultant, Cursor):
            # this implies, after operations, we deduce some value, like count, etc.
            # so we just return that resultant value
            return resultant

        if return_cursor:
            return resultant
        if return_generator:
            return self._results_generator(resultant)
        result_list = list(self._results_generator(resultant))
        return result_list

    def insert_one(self, doc):
        # print "Inserting",item
        if '_id' in doc and isinstance(doc['_id'], six.string_types):
            doc['_id'] = self.dbms_collection.object_id(doc['_id'])
        try:
            response = self.dbms_collection.insert_one(doc)
        except Exception as e:
            print("Error inserting into " + self.name + ": ", e)
            return None
        return response

    def insert_many(self, docs):
        for doc in docs:
            #  TODO copying before changing
            if '_id' in doc and isinstance(doc['_id'], six.string_types):
                doc['_id'] = self.dbms_collection.object_id(doc['_id'])
        try:
            response = self.dbms_collection.insert_many(docs)
        except Exception as e:
            print('Error inserting into {}'.format(self.name), e)
            return None
        return response

    def update_item(self, item_id, update_doc):
        query = self.query_doc_for_id(item_id)
        return self.update_one(query, update_doc)

    def update_one(self, query, update_doc, upsert=False):
        query = self.marshal_string_ids2object_ids(query)
        update_doc = self.marshal_string_ids2object_ids(update_doc)

        # print(query, update_doc)
        return self.dbms_collection.update_one(query, update_doc, upsert)

    def update_many(self, query, update_doc, upsert=False):
        query = self.marshal_string_ids2object_ids(query)
        update_doc = self.marshal_string_ids2object_ids(update_doc)

        return self.dbms_collection.update_many(query, update_doc, upsert)

    def delete_item(self, doc_id):
        query = self.query_doc_for_id(doc_id)
        return self.delete_one(query)

    def delete_one(self, query):
        if query is None:
            return 0
        query = self.marshal_string_ids2object_ids(query)
        return self.dbms_collection.delete_one(query)

    def delete_many(self, query):
        if query is None:
            return 0
        query = self.marshal_string_ids2object_ids(query)
        return self.dbms_collection.delete_many(query)

    def bulk_write(self, requests, ordered=True, bypass_document_validation=False, session=None):
        requests = self.marshal_string_ids2object_ids(requests)
        return self.dbms_collection.bulk_write(
            requests, ordered=ordered, bypass_document_validation=bypass_document_validation, session=session)

    def bulk_update(self, id_update_tuples, ordered=True):
        if not id_update_tuples:
            return None
        requests = [
            self.dbms_collection.operations.UpdateOne(
                self.query_doc_for_id(t[0]),
                self.marshal_string_ids2object_ids(t[1]),
            )
            for t in id_update_tuples
        ]
        return self.bulk_write(requests, ordered=ordered)

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        if query is None:
            return None
        query = self.marshal_string_ids2object_ids(query)
        update_doc = self.marshal_string_ids2object_ids(update_doc)

        result_doc = self.dbms_collection.find_one_and_update(query, update_doc, upsert=upsert, return_doc=return_doc)
        self.marshal_object_id2string_id(result_doc)
        return result_doc

    def drop(self):
        return self.dbms_collection.drop()

    def create_index(self, keys_dict, index_name):
        return self.dbms_collection.create_index(keys_dict, index_name)

    #  TODO add also other basic methods related to indexing

    # noinspection PyShadowingBuiltins
    def __exit__(self, type, value, traceback):
        return True


class MyDb:
    def __init__(self, dbms_db):
        self.dbms_db = dbms_db
        self.name = self.dbms_db.name
        self.c = {}

    def __getattr__(self, name):
        if name not in self.c:
            self._add_collection(name)
        return self.c[name]

    def __getitem__(self, name):
        return self.__getattr__(name)

    def __contains__(self, name):
        return name in self.c

    def _add_collection(self, cname, cache=False):
        dbms_collection = self.dbms_db.get_collection(cname)
        my_collection = MyDbCollection(dbms_collection, cache=cache)
        self.c[my_collection.name] = my_collection

    def get_collection(self, collection_name):
        return self.__getitem__(collection_name)

    # List all the collections / tables in the database
    def list(self):
        return self.c.keys()

    def list_collections(self):
        return self.dbms_db.list_collection_names()

    def drop(self):
        self.dbms_db.dbms_client.drop_database()


class MyDbClient(object):

    def __init__(self, dbms_client):
        self.dbms_client = dbms_client

    # should return MYDB/subclass instance for this client.
    def get_database(self, db_name):
        dbms_db = self.dbms_client.get_database(db_name)
        return MyDb(dbms_db)

    def list_database_names(self):
        return self.dbms_client.list_database_names()

    def drop_database(self, db_name):
        return self.dbms_client.drop_database(db_name)

    def close_connection(self):
        self.dbms_client.close()
