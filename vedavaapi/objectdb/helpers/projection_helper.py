from copy import deepcopy

from . import ObjModelException


def validate_projection(projection):
    if projection is None:
        return

    if not isinstance(projection, dict):
        raise ObjModelException('projection should be dict', 403)

    if not len(projection):
        return

    is_valid = False
    for p in (0, 1):
        if False not in [v == p for v in projection.values()]:
            is_valid = True
            break

    if not is_valid:
        raise ObjModelException('invalid projection', 403)


def modified_projection(projection, mandatory_attrs=None):
    mandatory_attrs = mandatory_attrs or []
    if projection is None:
        return None
    projection = projection.copy()

    if not len(projection):
        projection['_id'] = 1

    projection_mode = list(projection.values())[0]

    for attr in mandatory_attrs:
        if projection_mode == 0:
            projection.pop(attr, 0)
            if not len(projection):
                return None
        elif projection_mode == 1:
            projection[attr] = 1

    return projection


def get_restricted_projection(projection, exposed_projection):

    if exposed_projection is None:
        return projection.copy() if projection else projection
    if projection is None:
        return exposed_projection

    if 1 in exposed_projection.keys():
        resultant_projection = exposed_projection.copy()
        if 1 in projection.values():
            for k in resultant_projection:
                if k not in projection:
                    resultant_projection.pop(k, 1)
        else:
            for k in projection:
                resultant_projection.pop(k, 1)

    else:
        resultant_projection = projection.copy()
        if 1 in resultant_projection.values():
            for k in exposed_projection:
                resultant_projection.pop(k, 1)
        else:
            resultant_projection.update(exposed_projection)

        return resultant_projection


def delete_attr_if_not_requested(doc, attrs, projection, is_json=False):
    if projection is None:
        return

    projection_mode = list(projection.values())[0] if len(projection) else None

    if projection_mode == 0:
        for attr in attrs:
            if attr in projection:
                doc.pop(attr, None) if is_json else delattr(doc, attr)

    elif projection_mode == 1:
        for attr in attrs:
            if attr not in projection:
                doc.pop(attr, None) if is_json else delattr(doc, attr)


def project_doc(doc, projection, in_place=False):
    if doc is None or projection is None:
        return doc

    projection_mode = list(projection.values())[0] if len(projection) else None
    field_trees = [field.split('.') for field in projection]
    return project_doc_for_field_trees(doc, field_trees, projection_mode, in_place=in_place)


def project_doc_for_field_trees(doc, field_trees, mode, in_place=False):
    field_trees_map = {}
    # print(field_trees)
    for ft in field_trees:
        if ft[0] not in field_trees_map:
            field_trees_map[ft[0]] = []
        if len(ft) > 1:
            field_trees_map[ft[0]].append(ft[1:])
    # print(field_trees_map)

    projected_doc = deepcopy(doc) if not in_place else doc

    if mode == 0:
        for f, fts in field_trees_map.items():
            if f not in projected_doc:
                continue
            if not len(fts):
                projected_doc.pop(f)
                continue

            branch_doc = projected_doc.get(f)
            if isinstance(branch_doc, list):
                projected_branch_doc = [project_doc_for_field_trees(item, fts, 0) for item in branch_doc]
            else:
                projected_branch_doc = project_doc_for_field_trees(branch_doc, fts, 0)
            projected_doc[f] = projected_branch_doc

        return projected_doc

    elif mode == 1:
        for k in list(projected_doc.keys()):
            if k not in field_trees_map:
                projected_doc.pop(k)
                continue
            fts = field_trees_map.get(k)
            if not len(fts):
                continue

            branch_doc = projected_doc.get(k)
            if isinstance(branch_doc, list):
                projected_branch_doc = [project_doc_for_field_trees(item, fts, 1) for item in branch_doc]
            else:
                projected_branch_doc = project_doc_for_field_trees(branch_doc, fts, 1)
            projected_doc[k] = projected_branch_doc

        return projected_doc

    else:
        raise ObjModelException('invalid projection mode')


def get_nested_value_for_field_tree(doc, field_tree):
    if not field_tree:
        return doc
    first_field = field_tree[0]

    # print(doc, field_tree)
    if str.isnumeric(first_field):
        if not isinstance(doc, list):
            raise ObjModelException('invalid nested field tree', 400)
        first_level_value = doc[int(first_field)]

    else:
        if not isinstance(doc, dict):
            raise ObjModelException('invalid nested field tree', 400)
        first_level_value = doc.get(first_field, None)  # TODO None?

    return get_nested_value_for_field_tree(first_level_value, field_tree[1:])


def get_nested_value(doc, nested_field_string):
    field_tree = nested_field_string.split('.')
    return get_nested_value_for_field_tree(doc, field_tree)


def get_matched_nested_value_for_field_tree(doc, field_tree):
    if not field_tree:
        return doc
    first_field = field_tree[0]

    if str.isnumeric(first_field):
        if not isinstance(doc, list):
            raise ObjModelException('invalid nested field tree', 400)
        first_level_value = doc[int(first_field)]
        return get_matched_nested_value_for_field_tree(first_level_value, field_tree[1:])
    else:
        if isinstance(doc, dict):
            first_level_value = doc.get(first_field, None)  # TODO None?
            return get_matched_nested_value_for_field_tree(first_level_value, field_tree[1:])
        elif isinstance(doc, list):
            nested_values = []
            for item in doc:
                if not isinstance(item, dict):
                    continue
                first_level_value = item.get(first_field, None)
                nested_value = get_matched_nested_value_for_field_tree(first_level_value, field_tree[1:])
                if isinstance(nested_value, list):
                    nested_values.extend(nested_value)
                else:
                    nested_values.append(nested_value)
            try:
                nested_values = list(set(nested_values))
            except TypeError:
                pass
            return nested_values


def get_matched_nested_value(doc, nested_field_string):
    field_tree = nested_field_string.split('.')
    return get_matched_nested_value_for_field_tree(doc, field_tree)


def set_nested_value(doc, nested_field_string, value):
    field_tree = nested_field_string.split('.')
    if len(field_tree) == 1:
        doc[nested_field_string] = value
        return

    last_mutable_field_path = '.'.join(field_tree[: -1])
    last_mutable_value = get_nested_value(doc, last_mutable_field_path)

    #  print(last_mutable_field_path, last_mutable_value, doc)
    last_key = field_tree[-1]

    #  print({"ft": field_tree, "doc": doc, "lmfp": last_mutable_field_path, "lmv": last_mutable_value, "lk": last_key, "nfs": nested_field_string})

    if str.isnumeric(last_key):
        last_mutable_value[int(last_key)] = value
    else:
        last_mutable_value[last_key] = value


def get_primary_key_resolved_referred_links(doc, field, blank_referred_ids_to_resolved_ids_map):
    if not blank_referred_ids_to_resolved_ids_map:
        return doc.get(field, None)

    val = doc.get(field, None)
    if not val:
        return val
    if isinstance(val, str):
        return blank_referred_ids_to_resolved_ids_map.get(val, val)
    if isinstance(val, list):
        return [blank_referred_ids_to_resolved_ids_map.get(item, item) for item in val]
    else:
        return val


def get_primary_key_query_doc(doc, primary_key_set_list, blank_referred_ids_to_resolved_ids_map=None):
    or_list = []
    if '_id' in doc:
        or_list.append({"_id": doc['_id']})

    for primary_key_set in primary_key_set_list:
        if not primary_key_set:
            continue

        if 'jsonClass' not in primary_key_set:
            primary_key_set = primary_key_set + ['jsonClass']

        query_doc_for_key_set = {}
        for nf in primary_key_set:
            if nf in ('source', 'target'):
                query_doc_for_key_set[nf] = get_primary_key_resolved_referred_links(doc, nf, blank_referred_ids_to_resolved_ids_map)
            else:
                query_doc_for_key_set[nf] = get_nested_value(doc, nf)
        #  query_doc_for_key_set = dict((nf, get_nested_value(doc, nf)) for nf in primary_key_set)
        for k, v in query_doc_for_key_set.items():
            if not v:
                continue
        or_list.append(query_doc_for_key_set)

    return {"$or": or_list} if len(or_list) else None


#  TODO robust support for update_doc
