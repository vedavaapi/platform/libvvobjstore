import os
import shutil
import sys
from copy import deepcopy
import magic
import requests
from jsonschema import ValidationError

from vv_schemas import SchemaValidator

from ..helpers import json_object_helper
from ..mydb import MyDbCollection
from . import objstore_helper, projection_helper, ObjModelException


class GraphValidationError(Exception):
    def __init__(self, msg, error, http_status_code):
        super(GraphValidationError, self).__init__(msg)
        self.error = error,
        self.http_status_code = http_status_code


def _index_filter_to_mongo_query(index_filter: dict):
    if 'index_field' not in index_filter:
        return None
    # print(index_filter['index_field'])
    or_options = []
    if isinstance(index_filter.get('indices', None), list):
        or_options.append({
            index_filter['index_field']: {"$in": index_filter.get('indices')}
        })
    if isinstance(index_filter.get('range', None), list):
        # print('has range')
        for r in index_filter.get('range'):
            if not isinstance(r, dict):
                continue
            range_selector = {}
            if isinstance(r.get('from', None), (int, str)):
                range_selector['$gte'] = r['from']
            if isinstance(r.get('to', None), (int, str)):
                range_selector['$lte'] = r['to']
            or_options.append({
                index_filter['index_field']: range_selector
            })
    return {"$or": or_options} if len(or_options) else None


def _members_query_to_mongo_query(members_query):
    mongo_query = {}
    if isinstance(members_query.get('field_filter', None), dict):
        field_filter = deepcopy(members_query['field_filter'])
        if 'jsonClassName' in field_filter:
            field_filter['jsonClass'] = field_filter.pop('jsonClassName')
        mongo_query.update(field_filter)
    if isinstance(members_query.get('index_filters', None), list):
        index_filters_list = []
        for index_filter in members_query['index_filters']:
            mongo_query_by_index = _index_filter_to_mongo_query(index_filter)
            if mongo_query_by_index is None:
                continue
            index_filters_list.append(mongo_query_by_index)
        if len(index_filters_list):
            mongo_query['$and'] = mongo_query.get('$and', []) + index_filters_list
    return mongo_query or None


def resolve_members_query(colln, members_query, in_place=False):
    members_query = deepcopy(members_query) if not in_place else members_query
    mongo_query = _members_query_to_mongo_query(members_query)
    # print({"mongo_query": mongo_query})
    if not mongo_query:
        return
    matched_docs = colln.find(mongo_query, projection={"_id": 1})
    matched_ids = [d['_id'] for d in matched_docs]
    # print({"matched_ids": matched_ids})
    members_query['resource_ids'] = matched_ids
    return members_query


def validate_graph_structure(graph):
    if not isinstance(graph, dict):
        raise GraphValidationError('invalid graph', None, 400)

    for graph_id, node in graph.items():
        if not isinstance(node, dict):
            raise GraphValidationError(
                'invalid object definition for node with _id "{}"; node should be dict'.format(graph_id),
                None, 400
            )

        if 'jsonClass' not in node:
            raise GraphValidationError(
                'invalid object definition at node with _id "{}"; jsonClass should be supplied for a node'.format(graph_id),
                None, 400
            )


def validate_graph_ids_integrity(graph):
    for graph_id, node in graph.items():
        if '_id' not in node:
            node['_id'] = graph_id
            continue
        if graph_id != node['_id']:
            raise GraphValidationError('node with _id "{}" failed integrity check'.format(graph_id), None, 400)


def validate_graph_linking_integrity(graph):
    for graph_id, node in graph.items():
        if 'source' in node and 'target' in node:
            raise GraphValidationError('invalid node "{}"'.format(graph_id), None, 400)
        link_key, referred_ids = objstore_helper.get_referred_ids(node)
        if not link_key:
            return
            # raise GraphValidationError('node with _id "{}" is orphan'.format(graph_id), None, 400)

        for referred_obj_id in referred_ids:
            if referred_obj_id is None:
                continue
            if objstore_helper.is_blank_id(referred_obj_id) and referred_obj_id not in graph:
                raise GraphValidationError(
                    'node with blank_id "{}" does not exist in graph, but referred by other node with _id "{}"'.format(
                        referred_obj_id, graph_id
                    ),
                    None, 400
                )


def pop_blank_ids(graph):
    for graph_id, node in graph.items():
        if objstore_helper.is_blank_id(graph_id):
            node.pop("_id", None)


def resolve_primary_keys(colln, graph, vv_schemas, upsert=False):
    blank_ids_to_resolved_ids_map = {}

    for graph_id in list(graph.keys()):
        doc = graph[graph_id]
        if not objstore_helper.is_blank_id(graph_id):
            continue

        try:
            matched_resource_id_for_primary_key = objstore_helper.get_matched_resource_id_for_primary_key(
                colln, doc, vv_schemas,
                blank_referred_ids_to_resolved_ids_map=blank_ids_to_resolved_ids_map, skip_if_blank_links=True
            )
        except ObjModelException as e:
            raise GraphValidationError('invalid node with graph_id {}'.format(graph_id), e, 400)
        if not matched_resource_id_for_primary_key:
            continue

        if upsert:
            doc['_id'] = matched_resource_id_for_primary_key
            blank_ids_to_resolved_ids_map[graph_id] = matched_resource_id_for_primary_key
        else:
            raise GraphValidationError(
                'resource with matched primary keys exist for blank_id {}, with uid {}'.format(
                    graph_id, matched_resource_id_for_primary_key),
                None, 403
            )
    return blank_ids_to_resolved_ids_map


def validate_graph_nodes_upsert(
        colln, acl_svc, user_id, user_team_ids, graph, schema_validator: SchemaValidator,
        validate_schemas=True, validate_permissions=True, graph_cache=None):

    graph_cache = graph_cache if graph_cache is not None else {}
    report = {"link_updates": []}

    for graph_id, doc in graph.items():
        try:
            if '_id' in doc:
                if validate_schemas and schema_validator:
                    schema_validator.validate(doc, diff=True)
                res_report = objstore_helper.validate_update(
                    colln, acl_svc, doc, user_id, user_team_ids,
                    allow_blank_ids=True, validate_permissions=validate_permissions, graph_cache=graph_cache)
                if res_report.get('links_updated', None):
                    report['link_updates'].append(graph_id)
            else:
                if validate_schemas and schema_validator:
                    schema_validator.validate(doc)
                objstore_helper.validate_creation(
                    colln, acl_svc, doc, user_id, user_team_ids,
                    allow_blank_ids=True, validate_permissions=validate_permissions, graph_cache=graph_cache)
                report['link_updates'].append(graph_id)

        except ValidationError as e:
            raise GraphValidationError('schema validation error for node with _id "{}"'.format(graph_id), e, 400)

        except objstore_helper.ObjModelException as e:
            raise GraphValidationError(
                'integrity checks failed for operation on node with _id "{}"'.format(graph_id),
                e, e.http_response_code
            )
    return report


def get_ool_fields_to_oold_ids_map(value, path_string):
    ool_fields_to_oold_ids_map = {}

    if isinstance(value, str):
        if value.startswith('_OOLD:'):
            ool_fields_to_oold_ids_map.update(
                {path_string: value[6:]})

    elif isinstance(value, list):
        for i, item in enumerate(value):
            nl_path_string = '{}.{}'.format(path_string, str(i)) if path_string else str(i)
            f2gmap = get_ool_fields_to_oold_ids_map(item, nl_path_string)
            ool_fields_to_oold_ids_map.update(f2gmap)

    elif isinstance(value, dict):
        blocked_fields = ['jsonClass', 'source', 'target', 'hierarchy', '_id']
        for k, v in value.items():
            if k in blocked_fields:
                continue
            nl_path_string = '{}.{}'.format(path_string, k) if path_string else k
            f2gmap = get_ool_fields_to_oold_ids_map(v, nl_path_string)
            ool_fields_to_oold_ids_map.update(f2gmap)

    return ool_fields_to_oold_ids_map


def get_nodes_ool_fields_to_oold_ids_map(graph):
    nodes_ool_fields_to_oold_ids_map = {}
    for graph_id, node in graph.items():
        ool_fields_to_oold_ids_map = get_ool_fields_to_oold_ids_map(node, '')
        nodes_ool_fields_to_oold_ids_map[graph_id] = ool_fields_to_oold_ids_map
    return nodes_ool_fields_to_oold_ids_map


def validate_ool_field_values_integrity(colln, nodes_ool_fields_to_oold_ids_map, ool_data_graph):
    for graph_id, ool_fields_to_oold_ids_map in nodes_ool_fields_to_oold_ids_map.items():
        for field, oold_id in ool_fields_to_oold_ids_map.items():

            if objstore_helper.is_blank_id(oold_id):
                if oold_id not in ool_data_graph:
                    raise GraphValidationError(
                        'oold with blank_id {} is not defined in ool data graph, but included in node with _id {} at field {}'.format(
                            oold_id, graph_id, field),
                        None, 400
                    )

            else:
                oold_resource_json = colln.find_one({"_id": oold_id}, projection={"_id": 1})
                if not oold_resource_json:
                    raise GraphValidationError(
                        'oold with _id {} does not exists, but included in node with _id {} at field {}'.format(
                            oold_id, graph_id, field),
                        None, 400
                    )


def set_meta_attributes_for_graph_nodes(graph, vv_schemas):
    for graph_id, doc in graph.items():
        json_object_helper.set_type_field_recursively_for_doc(doc, vv_schemas)


def set_generated_attrs_for_graph_nodes(graph, user_id):
    for graph_id, doc in graph.items():
        if '_id' in doc:
            json_object_helper.update_time(doc, just_update=True)
        else:
            json_object_helper.update_time(doc)
            if user_id:
                doc['creator'] = user_id
        doc['contributor'] = [user_id]


def set_persisted_oold_ids(graph, nodes_ool_fields_to_oold_ids_map, oold_graph_ids_to_uids_map):
    for graph_id, node in graph.items():
        ool_field_to_oold_ids_map = nodes_ool_fields_to_oold_ids_map.get(graph_id, {})
        for field, oold_id in ool_field_to_oold_ids_map.items():
            if not objstore_helper.is_blank_id(oold_id):
                continue
            oold_uid = oold_graph_ids_to_uids_map[oold_id]
            projection_helper.set_nested_value(node, field, '_OOLD:{}'.format(oold_uid))


def push_graph_nodes_first_round(colln, graph):
    graph_ids_to_update = [graph_id for graph_id in graph.keys() if '_id' in graph[graph_id]]
    graph_ids_to_create = [graph_id for graph_id in graph.keys() if '_id' not in graph[graph_id]]

    #  print(graph_ids_to_create, graph_ids_to_update)

    updated_node_ids_map = {}
    res_id_updates_list = []
    for graph_id in graph_ids_to_update:
        n = graph[graph_id]
        res_id_updates_list.append((n['_id'], {'$set': n}))
        # colln.update_one({"_id": n['_id']}, {"$set": n})
        updated_node_ids_map[graph_id] = n['_id']

    if len(res_id_updates_list):
        colln.bulk_update(res_id_updates_list, ordered=False)

    nodes_to_create = [graph[graph_id] for graph_id in graph_ids_to_create]
    #  print(nodes_to_create)
    insert_result = colln.insert_many(nodes_to_create)
    inserted_ids = insert_result.inserted_ids if insert_result else []
    created_node_ids_map = dict(zip(graph_ids_to_create, inserted_ids))

    graph_ids_to_uids_map = {**created_node_ids_map, **updated_node_ids_map}

    return graph_ids_to_uids_map


def create_and_persist_default_acls(acl_svc, graph_id_to_uids_map, graph, user_id, initial_agents=None):
    acl_template = objstore_helper.get_default_acl(user_id, initial_agents)  # NOTE
    acls = []
    for graph_id, resource_id in graph_id_to_uids_map.items():
        if not objstore_helper.is_blank_id(graph_id):
            continue
        resource = graph[graph_id]

        link_key = objstore_helper.get_linking_key(resource)
        referrers = objstore_helper._get_normalized_nested_links(resource, link_key) if link_key else None
        if referrers:
            non_blank_links = [
                referrer for referrer in referrers
                if not objstore_helper.is_blank_id(referrer)
            ]
            if not non_blank_links:
                continue

        acl = acl_template.copy()
        acl['_id'] = resource_id
        acls.append(acl)
    if not len(acls):
        return
    acl_svc.colln.insert_many(acls)


def get_link_resolver_resource_updates_for_graph(graph, graph_ids_to_uids_map):
    resource_updates = {}

    for graph_id, node in graph.items():
        link_key, referred_ids = objstore_helper.get_referred_ids(node)
        if True not in [
            objstore_helper.is_blank_id(referred_id)
            for referred_id in referred_ids if referred_id is not None
        ]:
            resource_updates[graph_id] = None
            continue

        resource_update = {
            "_id": graph_ids_to_uids_map[graph_id]
        }

        if isinstance(node[link_key], list):
            resolved_referred_ids = [
                graph_ids_to_uids_map[referred_id]  # if objstore_helper.is_blank_id(referred_id) else referred_id
                for referred_id in referred_ids
            ]
            resource_update[link_key] = resolved_referred_ids
        else:
            referred_id = node[link_key]
            resolved_referred_id = (graph_ids_to_uids_map[referred_id])  # TODO ? if blank_id only
            resource_update[link_key] = resolved_referred_id

        resource_updates[graph_id] = resource_update

    return resource_updates


def validate_and_marshal_posted_graph(
        colln, acl_svc, user_id, user_team_ids, graph, schema_validator: SchemaValidator, upsert=False,
        validate_schemas=True, validate_permissions=True, graph_cache=None):

    graph_cache = graph_cache if graph_cache is not None else {}

    # 1. validate basic integrity of graph
    validate_graph_structure(graph)
    validate_graph_ids_integrity(graph)
    validate_graph_linking_integrity(graph)

    # 2. validate nodes, and operations over nodes, permissions.
    pop_blank_ids(graph)
    if schema_validator:
        resolve_primary_keys(
            colln, graph, schema_validator.vv_schemas,
            upsert=upsert
        )
        set_meta_attributes_for_graph_nodes(graph, schema_validator.vv_schemas)
    report = validate_graph_nodes_upsert(
        colln, acl_svc, user_id, user_team_ids, graph, schema_validator,
        validate_schemas=validate_schemas, validate_permissions=validate_permissions, graph_cache=graph_cache)
    set_generated_attrs_for_graph_nodes(graph, user_id)
    return report


def persist_graph(colln: MyDbCollection, acl_svc, graph, user_id, initial_agents=None):
    # 1. perform first round of pushing nodes to db
    graph_ids_to_uids_map = push_graph_nodes_first_round(colln, graph)
    create_and_persist_default_acls(
        acl_svc, graph_ids_to_uids_map, graph, user_id, initial_agents=initial_agents)
    # print(graph_ids_to_uids_map)

    # 2. perform second round of saving, to resolve linking
    link_resolver_resource_updates = get_link_resolver_resource_updates_for_graph(graph, graph_ids_to_uids_map)

    res_id_updates_list = [
        (update['_id'], {'$set': update}) for update in link_resolver_resource_updates.values() if update
    ]
    if len(res_id_updates_list):
        colln.bulk_update(res_id_updates_list, ordered=False)

    '''
    for resource_update in link_resolver_resource_updates.values():
        if resource_update is None:
            continue
        colln.update_one({"_id": resource_update['_id']}, {"$set": resource_update})
    '''

    # 3. return graph_ids_to_uids_map
    return graph_ids_to_uids_map


def validate_ool_data_graph_structure(ool_data_graph):
    validate_graph_structure(ool_data_graph)

    for graph_id, node in ool_data_graph.items():

        if not objstore_helper.is_blank_id(graph_id):
            raise GraphValidationError('only blank_ids allowed for ool_data_graph', None, 400)

        if '_id' in node and node['_id'] != graph_id:
            raise GraphValidationError('node with _id "{}" failed integrity check'.format(graph_id), None, 400)

        for k in ('namespace', 'identifier'):
            if not node.get(k, None):
                raise GraphValidationError(
                    'missing essential field {} in node with _id {} in ool_data_graph'.format(k, graph_id), None, 400)


def validate_and_marshal_ool_data_blank_id_linking(ool_data_graph, graph):
    for oold_graph_id, node in ool_data_graph.items():
        if 'source' not in node:
            raise GraphValidationError(
                'source field not set for oold_graph node with _id {}'.format(oold_graph_id), None, 400)

        node_source_id = node['source']
        if objstore_helper.is_blank_id(node_source_id):
            raise GraphValidationError(
                'oold graph nodes cannot refer to other oold nodes in graph. if they are blank ids, they should be in namespace _VV:', None, 400)
        if not node_source_id.startswith('_VV:'):
            continue

        node_source_id = node_source_id[4:]
        if not objstore_helper.is_blank_id(node_source_id):
            raise GraphValidationError(
                'linking attributes should not specify graph namespace unless they are blank_ids. error at oold graph node with blank_id {}'.format(oold_graph_id), None, 400)

        if node_source_id not in graph:
            raise GraphValidationError(
                'node with blank_id {} not defined in graph, but referred in oold_graph node with _id {}'.format(node_source_id, oold_graph_id), None, 400
            )

        node['source'] = node_source_id


def validate_and_set_oold_vedavaapi_identifiers(ool_data_graph):
    for graph_id, node in ool_data_graph.items():
        if node['namespace'] != '_vedavaapi':
            continue
        if 'identifier' in node and node['identifier'] != graph_id:
            return GraphValidationError('invalid identifier in ool data node {}'.format(graph_id), None, 400)
        node['identifier'] = graph_id


def validate_remote_file(node_id, url):
    """
    @see https://requests.readthedocs.io/en/master/user/advanced/#body-content-workflow
    """
    with requests.get(url, stream=True) as r:
        try:
            r.raise_for_status()
        except requests.HTTPError as e:
            raise GraphValidationError(
                f'error in de-referencing remote file referenced by oold node {node_id}', e, 400)
        except Exception as e:
            raise GraphValidationError(f'oold file {node_id} refers invalid remote url', e, 400)


        content_length = r.headers.get('content-length')
        if content_length is None:
            #  TODO
            return
        if not content_length or int(content_length) > 512 * 1024 * 1024:
            raise GraphValidationError(
                f'remote file referenced by oold node {node_id} exceeds size limit', None, 400)


def validate_ool_data_files_integrity(ool_data_graph, files_map):
    for graph_id, node in ool_data_graph.items():
        if node['namespace'] != '_vedavaapi':
            continue
        identifier = node['identifier']
        proto = node.get('proto')
        if not proto:
            if identifier not in files_map:
                raise GraphValidationError(
                    'file with name {} not attached, but defined in ool_data_graph node with _id {}'.format(
                        identifier, graph_id), None, 400
                )
        elif proto == 'http':
            validate_remote_file(graph_id, identifier)
        else:
            # TODO should support _abstract proto
            raise GraphValidationError(f'invalid proto in oold node {graph_id}', None, 400)


def set_vedavaapi_namespace_data(node, dir_path, file_id, file_name):
    node['namespaceData'] = {
        "jsonClass": "VedavaapiOOLDNamespaceData",
        "namespace": "_vedavaapi"
    }

    namespace_data = node['namespaceData']
    namespace_data['name'] = file_name

    magic_handler = magic.Magic(mime=True)
    namespace_data['mimetype'] = magic_handler.from_file(os.path.join(dir_path, file_id))
    # namespace_data['contentType'] = file.content_type


def validate_and_marshal_posted_ool_data_graph(
        colln, acl_svc, ool_data_graph, files_map, graph, user_id, team_ids, schema_validator: SchemaValidator,
        validate_schemas=True, validate_permissions=True, graph_cache=None):
    graph_cache = graph_cache if graph_cache is not None else {}

    validate_ool_data_graph_structure(ool_data_graph)
    validate_and_marshal_ool_data_blank_id_linking(ool_data_graph, graph)
    validate_ool_data_files_integrity(ool_data_graph, files_map)

    pop_blank_ids(ool_data_graph)
    if schema_validator:
        set_meta_attributes_for_graph_nodes(ool_data_graph, schema_validator.vv_schemas)
    validate_and_set_oold_vedavaapi_identifiers(ool_data_graph)
    report = validate_graph_nodes_upsert(
        colln, acl_svc, user_id, team_ids, ool_data_graph, schema_validator,
        validate_schemas=validate_schemas, validate_permissions=validate_permissions, graph_cache=graph_cache)
    set_generated_attrs_for_graph_nodes(
        ool_data_graph, user_id
    )
    return report


def get_vedavaapi_namespace_resolver_updates_for_ool_data_graph(ool_data_graph, graph_ids_to_uids_map, dir_path):
    resource_updates = {}

    for graph_id, node in ool_data_graph.items():
        if node['namespace'] != '_vedavaapi':
            resource_updates[graph_id] = None
            continue

        uid = graph_ids_to_uids_map[graph_id]
        resource_update = {
            "_id": uid,
            "identifier": uid,
            "url": "files/{}".format(uid)
        }
        if 'proto' in node:
            resource_update['proto'] = None
        set_vedavaapi_namespace_data(resource_update, dir_path, uid, node['identifier'])
        resource_updates[graph_id] = resource_update

    return resource_updates


def get_ool_data_graph_linking_resolver_update(ool_data_graph, main_graph_ids_to_uids_map, oold_graph_ids_to_uids_map):
    resource_updates = {}
    for oold_graph_id, node in ool_data_graph.items():
        oold_uid = oold_graph_ids_to_uids_map[oold_graph_id]
        node_source_id = node['source']
        if not objstore_helper.is_blank_id(node_source_id):
            resource_updates[oold_graph_id] = None
            continue

        # node_source_id = node_source_id[4:]
        node_source_created_uid = main_graph_ids_to_uids_map[node_source_id]
        resource_update = {
            "_id": oold_uid,
            "source": node_source_created_uid
        }
        resource_updates[oold_graph_id] = resource_update

    return resource_updates


def download_file(url, fp):
    # TODO should cache mimetype? gzip support? size-re-check?
    # @see https://stackoverflow.com/a/39217788/5925119
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        with open(fp, 'wb') as f:
            shutil.copyfileobj(r.raw, f)


def save_ool_data_files(ool_data_graph, graph_ids_to_uid_map, files_map, dir_path):
    if dir_path and not os.path.isdir(dir_path):
        try:
            os.makedirs(str(dir_path).rstrip('/') + '/', exist_ok=True)
        except Exception as e:
            raise GraphValidationError('cannot persist files', None, 500)
    for graph_id, node in ool_data_graph.items():
        if node['namespace'] != '_vedavaapi':
            continue
        if dir_path is None:
            raise GraphValidationError('no path specified for storing files', None, 500)
        identifier = node['identifier']
        file_path = os.path.join(dir_path, graph_ids_to_uid_map[graph_id])

        proto = node.get('proto')
        if not proto:
            file = files_map[identifier]
            file.save(file_path)
        elif proto == 'http':
            try:
                download_file(identifier, file_path)
            except:
                pass


def persist_ool_data_graph(colln, acl_svc, dir_path, ool_data_graph, files_map, user_id, initial_agents=None):
    # 1. perform first round of pushing nodes to db
    graph_ids_to_uids_map = push_graph_nodes_first_round(colln, ool_data_graph)
    create_and_persist_default_acls(acl_svc, graph_ids_to_uids_map, ool_data_graph, user_id, initial_agents=initial_agents)

    # 2. save local namespace ool data files
    save_ool_data_files(
        ool_data_graph, graph_ids_to_uids_map, files_map, dir_path
    )

    # 3. perform second round operation to resolve local identifiers
    local_namespace_resolver_update = get_vedavaapi_namespace_resolver_updates_for_ool_data_graph(
        ool_data_graph, graph_ids_to_uids_map, dir_path)

    res_id_updates_list = [
        (update['_id'], {'$set': update}) for update in local_namespace_resolver_update.values() if update
    ]
    if len(res_id_updates_list):
        colln.bulk_update(res_id_updates_list, ordered=False)

    '''
    for resource_update in local_namespace_resolver_update.values():
        if resource_update is None:
            continue
        colln.update_one({"_id": resource_update['_id']}, {"$set": resource_update})
    '''

    return graph_ids_to_uids_map


def update_oold_links(colln, ool_data_graph, main_graph_ids_to_uids_map, oold_graph_ids_to_uids_map):
    link_resolver_resource_updates = get_ool_data_graph_linking_resolver_update(
        ool_data_graph, main_graph_ids_to_uids_map, oold_graph_ids_to_uids_map)

    res_id_updates_list = [
        (update['_id'], {'$set': update}) for update in link_resolver_resource_updates.values() if update
    ]
    if len(res_id_updates_list):
        colln.bulk_update(res_id_updates_list, ordered=False)


def  get_hierarchy_update_defs(
        graph, graph_ids_to_uids_map, main_hmap,
        oold_graph, link_upserted_gids, graph_cache
):
    main_hmap = main_hmap or {}
    hierarchy_updates = {}
    link_updated_ids = []
    is_oold = oold_graph is not None

    def set_hierarchy(res_id):
        if res_id in hierarchy_updates:
            return hierarchy_updates[res_id]
        res = (oold_graph if is_oold else graph)[res_id]
        if not objstore_helper.is_blank_id(res_id):
            link_updated_ids.append(res_id)

        link_key, referred_ids = objstore_helper.get_referred_ids(res)
        if not link_key:
            hierarchy_updates[res_id] = []
            return []
        hierarchy = []
        # print(res_id, res, link_key, referred_ids)
        for i, rid in enumerate(referred_ids):
            # print(rid)
            if rid.startswith('_VV:'):
                rid = rid[4:]
            if is_oold and objstore_helper.is_blank_id(rid) and rid not in main_hmap:
                # blank, but primary-key matched referrer
                rid = graph_ids_to_uids_map[rid]

            if objstore_helper.is_blank_id(rid):
                ref_hierarchy = main_hmap[rid] if is_oold else set_hierarchy(rid)
                ref = {'i': graph_ids_to_uids_map[rid], 'c': graph[rid]['jsonClass'], 'p': [i], 'e': 1}
            else:
                #  print(graph_cache[rid])
                ref_hierarchy = main_hmap.get(rid, None) or graph_cache[rid].get('hierarchy', [])
                ref = {'i': rid, 'c': graph_cache[rid]['jsonClass'], 'p': [i], 'e': 1}
            hierarchy.append(ref)
            for hitem in ref_hierarchy or []:
                hitem = hitem.copy()
                hitem['p'] = [i] + hitem['p']
                hitem['e'] = len(hitem['p'])
                hierarchy.append(hitem)
        hierarchy_updates[res_id] = hierarchy
        return hierarchy

    for luid in link_upserted_gids:
        set_hierarchy(luid)

    return hierarchy_updates, link_updated_ids

def update_hierarchy(colln, graph, graph_ids_to_uids_map, link_upserted_gids, graph_cache):
    hierarchy_updates, link_updated_ids = get_hierarchy_update_defs(
        graph, graph_ids_to_uids_map, {},
        None, link_upserted_gids, graph_cache
    )
    updates_list = [
        (graph_ids_to_uids_map[res_id], {'$set': {'hierarchy': hierarchy}}) for res_id, hierarchy in hierarchy_updates.items()
    ]
    if len(updates_list):
        colln.bulk_update(updates_list, ordered=False)

    if len(link_updated_ids):
        for luid in link_updated_ids:
            objstore_helper.update_referrer_hierarchies(colln, graph_ids_to_uids_map[luid], hierarchy_updates[luid])
    return hierarchy_updates

def update_oold_hierarchy(colln, graph, graph_ids_to_uids_map, main_hmap, oold_graph, oold_gids_to_uids_map, link_upserted_gids, graph_cache):
    hierarchy_updates, link_updated_ids = get_hierarchy_update_defs(
        graph, graph_ids_to_uids_map, main_hmap,
        oold_graph, link_upserted_gids, graph_cache
    )
    updates_list = [
        (oold_gids_to_uids_map[res_id], {'$set': {'hierarchy': hierarchy}}) for res_id, hierarchy in hierarchy_updates.items()
    ]
    if len(updates_list):
        colln.bulk_update(updates_list, ordered=False)

    if len(link_updated_ids):
        for luid in link_updated_ids:
            objstore_helper.update_referrer_hierarchies(colln, graph_ids_to_uids_map[luid], hierarchy_updates[luid])
    return hierarchy_updates


def post_graph_with_ool_data(
        colln, acl_svc, dir_path, user_id, team_ids,
        graph, ool_data_graph, files_map, schema_validator: SchemaValidator,
        initial_agents=None, upsert=False, validate_schemas=True, validate_permissions=True):

    oold_graph_cache = {}
    graph_cache = {}

    # 1. validate ool_data_graph, and files
    oold_validation_report = validate_and_marshal_posted_ool_data_graph(
        colln, acl_svc, ool_data_graph, files_map, graph, user_id, team_ids, schema_validator,
        validate_schemas=validate_schemas, validate_permissions=validate_permissions, graph_cache=oold_graph_cache
    )

    # 2. validate main graph
    graph_validation_report = validate_and_marshal_posted_graph(
        colln, acl_svc, user_id, team_ids, graph, schema_validator, upsert=upsert,
        validate_schemas=validate_schemas, validate_permissions=validate_permissions, graph_cache=graph_cache
    )

    # 3. validate main graph's oold linking
    nodes_ool_fields_to_oold_ids_map = get_nodes_ool_fields_to_oold_ids_map(graph)

    validate_ool_field_values_integrity(colln, nodes_ool_fields_to_oold_ids_map, ool_data_graph)

    # 4. persist ool_data_graph, and ool data files if any
    ool_data_graph_ids_to_uids_map = persist_ool_data_graph(
        colln, acl_svc, dir_path, ool_data_graph, files_map, user_id, initial_agents=initial_agents)

    # 5. update graph nodes' oold ids to persisted ids
    set_persisted_oold_ids(graph, nodes_ool_fields_to_oold_ids_map, ool_data_graph_ids_to_uids_map)

    # 6. persist main graph
    graph_ids_to_uids_map = persist_graph(colln, acl_svc, graph, user_id, initial_agents=initial_agents)

    # 7. update ool_data graph links
    update_oold_links(colln, ool_data_graph, graph_ids_to_uids_map, ool_data_graph_ids_to_uids_map)

    #  print('link_updates', graph_validation_report['link_updates'])

    hierarchy_updates = update_hierarchy(
        colln, graph, graph_ids_to_uids_map, graph_validation_report['link_updates'], graph_cache
    )

    # print(graph_ids_to_uids_map, ool_data_graph_ids_to_uids_map, hierarchy_updates, graph_cache.keys())
    graph_cache.update(oold_graph_cache)

    update_oold_hierarchy(
        colln, graph, graph_ids_to_uids_map, hierarchy_updates,
        ool_data_graph, ool_data_graph_ids_to_uids_map, oold_validation_report['link_updates'], graph_cache
    )

    # 8. return graph_ids_to_uids_maps
    return graph_ids_to_uids_map, ool_data_graph_ids_to_uids_map


def get_projected_graph_from_ids_map(colln, graph_ids_to_uids_map, json_class_projection_map):
    graph = dict(
        (graph_id, colln.find_one({"_id": uid}))
        for (graph_id, uid) in graph_ids_to_uids_map.items()
    )
    project_graph_nodes(graph, json_class_projection_map, in_place=True)

    return graph


"""
Graph Query Helpers
"""


def _simple_merge_in_filter_docs(fd1, fd2):
    result = deepcopy(fd1)
    fd2_copy = deepcopy(fd2)

    if not ('$in' in result and '$in' in fd2):
        result.update(fd2_copy)
    else:
        result['$in'] = list(set(result['$in']).intersection(fd2_copy.pop('$in')))
        result.update(fd2_copy)
    return result


def _resolve_members_query_in_doc(colln, doc, key_path):
    try:
        members_query = projection_helper.get_nested_value(doc, key_path)
        if not members_query:
            return
    except ObjModelException:
        return
    resolve_members_query(colln, members_query, in_place=True)


def resolve_includable_ids(hop_inclusion_config, node_ids, graph):
    if not hop_inclusion_config:
        return []

    elif isinstance(hop_inclusion_config, dict):

        try:
            projection_helper.validate_projection(hop_inclusion_config)
        except ObjModelException as e:
            raise GraphValidationError(e.message, e, 400)

        mode = list(hop_inclusion_config.values())[0]
        json_class_names = hop_inclusion_config.keys()

        if mode == 1:
            return [
                node_id for node_id in node_ids
                if node_id in graph and graph[node_id]['jsonClass'] in json_class_names
            ]
        else:
            return [
                node_id for node_id in node_ids
                if node_id in graph and graph[node_id]['jsonClass'] not in json_class_names
            ]

    elif callable(hop_inclusion_config):
        includable_node_ids = []
        for node_id in node_ids:
            node = graph[node_id]
            try:
                should_include = hop_inclusion_config(node)
            except:
                should_include = False
            if should_include:
                includable_node_ids.append(node_id)
        return includable_node_ids

    else:
        return node_ids[:]

def get_graph(
        colln, acl_svc, start_nodes,
        traverse_key_filter_maps_list, direction, max_hops, user_id, team_ids,
        traversed_graph=None, acls_cache=None,
        include_incomplete_paths=False, hop_inclusions_config=None
        ):

    traversed_graph = traversed_graph if traversed_graph is not None else {}
    acls_cache = acls_cache if acls_cache is not None else {}

    hop_inclusions_config = hop_inclusions_config or [True]
    if isinstance(hop_inclusions_config[0], str):
        try:
            hop_inclusions_config[0] = eval(hop_inclusions_config[0])
        except SyntaxError as e:
            raise GraphValidationError('invalid hop inclusion config', str(e), 400)

    traverse_key_filter_maps_list = traverse_key_filter_maps_list or [{"source": {}, "target": {}}]

    cumulative_collected_node_ids = []
    traversed_start_node_ids = []

    start_nodes_map = dict((node['_id'], node) for node in start_nodes)
    start_nodes_ids = [node['_id'] for node in start_nodes]

    traversed_graph.update(start_nodes_map)
    if max_hops == 0 or not len(start_nodes_ids):
        includable_node_ids = resolve_includable_ids(hop_inclusions_config[0], start_nodes_ids, traversed_graph)
        cumulative_collected_node_ids.extend(
            [node_id for node_id in includable_node_ids if node_id not in cumulative_collected_node_ids])
        #  if len(start_nodes_ids):
        return traversed_graph, acls_cache, start_nodes_ids, cumulative_collected_node_ids

    key_filter_map = traverse_key_filter_maps_list[0]

    node_id_reached_ids_map = {}
    for node_id in start_nodes_ids:
        node_id_reached_ids_map[node_id] = {}

    or_options = []

    def _get_normalized_nested_links(doc, nested_field):
        try:
            v = projection_helper.get_nested_value(doc, nested_field) or []
        except ObjModelException:
            v = []
        if not isinstance(v, (str, list)):
            return None
        return {
            str: lambda attr: [attr],
            list: lambda attr: attr,
        }[type(v)](v)

    if direction == 'referred':
        for k, filter_doc in key_filter_map.items():
            selector_doc = filter_doc.copy()
            all_nodes_referred_ids = set()

            for node_id in start_nodes_ids:
                reached_ids_map = node_id_reached_ids_map[node_id]
                node = start_nodes_map[node_id]
                doc_referred_ids = _get_normalized_nested_links(node, k)
                if doc_referred_ids is None:
                    continue
                reached_ids_map[k] = doc_referred_ids
                all_nodes_referred_ids.update(doc_referred_ids)

            if '_id' not in selector_doc:
                selector_doc.update({"_id": {}})
            elif not isinstance(selector_doc['_id'], dict):
                selector_doc['_id'] = {"$eq": selector_doc['_id']}
            selector_doc['_id'] = _simple_merge_in_filter_docs(
                {"$in": list(all_nodes_referred_ids)}, selector_doc['_id'])
            or_options.append(selector_doc)

    elif direction == 'referrer':
        for k, filter_doc in key_filter_map.items():
            selector_doc = filter_doc.copy()
            if k not in selector_doc:
                selector_doc.update({k: {}})
            elif not isinstance(selector_doc[k], dict):
                selector_doc[k] = {"$eq": selector_doc[k]}
            selector_doc[k] = _simple_merge_in_filter_docs({"$in": start_nodes_ids}, selector_doc[k])
            or_options.append(selector_doc)

    if len(or_options):
        or_selector_doc = {
            "$or": or_options
        }
        reached_nodes = objstore_helper.get_read_permitted_resources(
            colln, acl_svc, user_id, team_ids, or_selector_doc,
            projection=None, resource_graph_cache=traversed_graph, acls_cache=acls_cache)
    else:
        reached_nodes = []

    if direction == 'referrer':
        for rn in reached_nodes:
            for k in key_filter_map:
                links = _get_normalized_nested_links(rn, k)
                if links is None:
                    continue
                for link in links:
                    if link in node_id_reached_ids_map:
                        reached_ids_map = node_id_reached_ids_map[link]
                        if k not in reached_ids_map:
                            reached_ids_map[k] = []
                        reached_ids_map[k].append(rn['_id'])

        for node_id in start_nodes_map:
            node = start_nodes_map[node_id]
            reached_ids_map = node_id_reached_ids_map[node_id]
            if isinstance(node.get('_order'), list):
                order = node['_order']
                key_fn = lambda rid: order.index(rid) + 1 or 1000000
                for k in reached_ids_map:
                    reached_ids_map[k] = sorted(reached_ids_map[k], key=key_fn)

    # noinspection PyTupleAssignmentBalance
    traversed_graph, acls_cache, nh_traversed_start_node_ids, nh_cumulative_collected_node_ids = get_graph(
        colln, acl_svc, reached_nodes,
        traverse_key_filter_maps_list[1:] if len(traverse_key_filter_maps_list) > 1 else traverse_key_filter_maps_list,
        direction, max_hops - 1, user_id, team_ids,
        traversed_graph=traversed_graph, acls_cache=acls_cache, include_incomplete_paths=include_incomplete_paths,
        hop_inclusions_config=hop_inclusions_config[1:] if len(hop_inclusions_config) > 1 else hop_inclusions_config)

    for node_id in start_nodes_ids:
        reached_ids_map = node_id_reached_ids_map[node_id]
        is_incomplete_traversal = True if max_hops > 0 else False  # can be direct
        if is_incomplete_traversal:
            for k, reached_ids in reached_ids_map.items():
                if len(reached_ids) and max_hops == 1:
                    is_incomplete_traversal = False
                    break
                for reached_id in reached_ids[:]:
                    if reached_id not in nh_traversed_start_node_ids:
                        reached_ids.remove(reached_id)
                    else:
                        is_incomplete_traversal = False
                        break
        if not is_incomplete_traversal or include_incomplete_paths:
            start_nodes_map[node_id]['_reached_ids'] = reached_ids_map
            traversed_start_node_ids.append(node_id)
            includable_node_ids = resolve_includable_ids(hop_inclusions_config[0], [node_id], traversed_graph)
            cumulative_collected_node_ids.extend(includable_node_ids)

            reached_atleast_one = False
            for k, reached_ids in reached_ids_map.items():
                if len(reached_ids):
                    reached_atleast_one = True

    cumulative_collected_node_ids.extend(nh_cumulative_collected_node_ids)

    return traversed_graph, acls_cache, traversed_start_node_ids, cumulative_collected_node_ids


def get_ool_data_graph(colln, acl_svc, graph, user_id, team_ids):
    ool_data_ids = set()
    nodes_oold_fields_to_oold_ids_map = get_nodes_ool_fields_to_oold_ids_map(graph)

    for node_id, oold_fields_to_oold_ids_map in nodes_oold_fields_to_oold_ids_map.items():
        ool_data_ids.update(oold_fields_to_oold_ids_map.values())

    selector_doc = {
        "_id": {"$in": list(ool_data_ids)}
    }
    ool_data_jsons = objstore_helper.get_read_permitted_resources(
        colln, acl_svc, user_id, team_ids, selector_doc, projection=None)

    return dict((node['_id'], node) for node in ool_data_jsons)


def project_graph_nodes(graph, json_class_projection_map, in_place=False):

    def get_projection(jc):
        matched_projection = json_class_projection_map.get(jc, None)
        try:
            projection_helper.validate_projection(matched_projection)
        except ObjModelException:
            matched_projection = None

        '''
        
        matched_projection = projection_helper.modified_projection(
            matched_projection, mandatory_attrs=['resolvedPermissions'])
        '''
        return matched_projection

    default_projection = get_projection('*')
    if not in_place:
        graph = graph.copy()

    for uid, node in graph.items():
        if not in_place:
            node = node.copy()
        json_class = node['jsonClass']
        projection = get_projection(json_class) if json_class in json_class_projection_map else default_projection
        node = projection_helper.project_doc(node, projection, in_place=in_place)
        if not in_place:
            graph[uid] = node

    return graph


"""
Network Query Helpers
"""


def get_network(
        colln, acl_svc, start_nodes_selector, start_find_ops, traversed_network, edge_filters_list,
        from_link_field, to_link_field, max_hops, user_id, team_ids, in_place=False):

    start_nodes = objstore_helper.get_read_permitted_resources(
        colln, acl_svc, user_id, team_ids, start_nodes_selector,
        projection=None, ops=start_find_ops)
    start_nodes_map = dict((node['_id'], node) for node in start_nodes)
    start_nodes_ids = [node['_id'] for node in start_nodes]

    if not in_place:
        traversed_network = traversed_network.copy()
        for k in traversed_network:
            traversed_network[k] = traversed_network[k].copy()

    traversed_network['nodes'].update(start_nodes_map)

    if max_hops <= 0 or not len(start_nodes_ids):
        return traversed_network, start_nodes_ids

    if not edge_filters_list:
        edge_filters_list = [{}]

    edges_selector_doc = edge_filters_list[0].copy()
    for f in (from_link_field, to_link_field):
        if f not in edges_selector_doc or not isinstance(edges_selector_doc[f], dict):
            edges_selector_doc[f] = {}

    edges_selector_doc[from_link_field].update({
        "$in": start_nodes_ids
    })
    edges_selector_doc[to_link_field].update({
        "$type": [2, 4]  # string/array respectively
    })

    #  print({"esd": edges_selector_doc}, {"lf": [from_link_field, to_link_field]})

    edges = objstore_helper.get_read_permitted_resources(
        colln, acl_svc, user_id, team_ids, edges_selector_doc,
        projection=None)
    edges_map = dict((edge['_id'], edge) for edge in edges)
    #  print(edges_map)

    next_hop_edge_filter_index = 1 if len(edge_filters_list) > 1 else 0
    next_hop_edge_filters_list = edge_filters_list[next_hop_edge_filter_index:]

    # we are doing separate queries because, we have to ensure other node/s is readable before we include each edge
    for edge_id, edge in edges_map.items():
        # print({"edge_id": edge_id})
        to_attr = projection_helper.get_matched_nested_value(edge, to_link_field)
        #  print({"to_attr": to_attr})
        if not to_attr:
            continue
        to_node_ids = to_attr if isinstance(to_attr, list) else [to_attr]
        #  print({"to_node_ids": to_node_ids})
        next_hop_start_docs_selector = {
            "_id": {
                "$in": to_node_ids
            }
        }
        next_hop_traversed_network, next_hop_start_nodes_ids = get_network(
            colln, acl_svc, next_hop_start_docs_selector, None,
            {"nodes": {}, "edges": {}}, next_hop_edge_filters_list,
            from_link_field, to_link_field, max_hops-1, user_id, team_ids, in_place=True)
        if not len(next_hop_start_nodes_ids):
            continue

        traversed_network['nodes'].update(next_hop_traversed_network['nodes'])
        traversed_network['edges'].update(next_hop_traversed_network['edges'])
        traversed_network['edges'][edge_id] = edge

    return traversed_network, start_nodes_ids
