from datetime import datetime
import six

def update_time(doc, just_update=False):
    now = datetime.now()
    time_attr = 'modified' if (just_update or ('created' in doc)) else 'created'
    doc[time_attr] = str(now)

def _set_type_field_recursively_for_doc(doc, vv_schemas):
    json_class = doc.get('jsonClass', None)
    vv_schema = vv_schemas.get(json_class)
    if not vv_schema:
        pass
    else:
        doc['type'] = vv_schema.get('properties', {}).get('type', {}).get('enum', ['owl:Thing'])[0]

    for k, v in doc.items():
        if isinstance(v, dict):
            set_type_field_recursively_for_doc(v, vv_schemas)
        elif isinstance(v, list):
            for item in v:
                if isinstance(item, dict):
                    set_type_field_recursively_for_doc(item, vv_schemas)


def set_type_field_recursively_for_doc(doc, vv_schemas):
    if isinstance(doc, dict):
        json_class = doc.get('jsonClass', None)
        if not json_class or not isinstance(json_class, six.string_types):
            pass
        else:
            if json_class == 'VVSchema':
                return
            vv_schema = vv_schemas.get(json_class)
            if vv_schema:
                schema = vv_schema
                doc_type = schema.get('properties', {}).get('type', {}).get('enum', [None])[0]
                if doc_type:
                    doc['type'] = doc_type

        for k, v in doc.items():
            set_type_field_recursively_for_doc(v, vv_schemas)
    elif isinstance(doc, list):
        for item in doc:
            set_type_field_recursively_for_doc(item, vv_schemas)
    else:
        return
