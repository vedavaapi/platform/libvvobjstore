
class ObjModelException(BaseException):

    def __init__(self, message, http_response_code=None, attachments=None):
        super(ObjModelException, self).__init__(message)
        self.message = message
        self.http_response_code = http_response_code
        self.attachments = attachments
