import json
import os
from copy import deepcopy

from ..helpers import json_object_helper
from vv_schemas import SchemaValidator

from . import ObjModelException
from . import projection_helper
from vedavaapi.acls.permissions_helper import PermissionResolver, AclsGenerator  # NOTE this.


def resource_selector_doc(resource_id):
    return {"_id": resource_id}


def specific_resources_selector_doc(source_id, custom_filter_doc=None):
    selector_doc = custom_filter_doc.copy() if custom_filter_doc else {}
    selector_doc.update({
        "source": source_id
    })
    return selector_doc


def annotations_selector_doc(target_id, custom_filter_doc=None):
    selector_doc = custom_filter_doc.copy() if custom_filter_doc else {}
    selector_doc.update({
        "target": target_id
    })
    return selector_doc


def get_in_links(colln, resource_id):
    selector_doc = {
        "$or": [
            {
                "source": resource_id
            },
            {
                "target": resource_id
            }
        ]
    }
    children = colln.find(selector_doc, projection={"_id": 1, "source": 1, "target": 1})
    in_links = {"source": [], "target": []}
    for child in children:
        arr = in_links['source' if 'source' in child else 'target']
        arr.append(child['_id'])

    return in_links


def get_read_permitted_resources(
        colln, acl_svc, user_id, team_ids, selector_doc, projection=None, ops=None,
        attach_in_links=False, resource_graph_cache=None, acls_cache=None):

    read_permitted_resources = []
    matched_resources = colln.find_and_do(
        selector_doc, ops if ops is not None else {},
        projection=None
    )
    resolved_permissions_list = PermissionResolver.get_resolved_permissions_for_resources(
        colln, acl_svc, matched_resources, PermissionResolver.ACTIONS, user_id, team_ids,
        resource_graph_cache=resource_graph_cache, acls_cache=acls_cache)

    for i, r in enumerate(matched_resources):
        resolved_permissions = resolved_permissions_list[i]
        if not resolved_permissions[PermissionResolver.READ]:
            continue

        r['resolvedPermissions'] = resolved_permissions
        r_id = r['_id']
        projection_helper.project_doc(r, projection, in_place=True)

        if attach_in_links:
            in_links = get_in_links(colln, r_id)
            r['_in_links'] = in_links

        read_permitted_resources.append(r)

    return read_permitted_resources


def get_resource(colln, acl_svc, selector_doc, user_id, team_ids):

    resource = colln.find_one(selector_doc, projection=None)
    if not resource:
        raise ObjModelException('resource does not exist', 404)

    resolved_permissions = PermissionResolver.get_resolved_permissions(
        colln, acl_svc, resource, PermissionResolver.ACTIONS, user_id, team_ids)
    resource['resolvedPermissions'] = resolved_permissions

    return resource


def get_referred_ids(res):
    link_key = get_linking_key(res)
    if not link_key:
        return None, []
    link_attr = res[link_key]
    referred_ids = link_attr if isinstance(link_attr, list) else [link_attr]
    return link_key, referred_ids


def get_linking_key(resource_json):
    if 'source' in resource_json:
        return 'source'
    elif 'target' in resource_json:
        return 'target'
    return None


def is_blank_id(_id):
    return _id.startswith('_:')


def validate_creation(
        colln, acl_svc, resource, user_id, team_ids, not_allowed_attributes=None, allow_blank_ids=False,
        validate_permissions=True, graph_cache=None):

    not_allowed_attributes = list(not_allowed_attributes or [])
    not_allowed_attributes.extend(['creator', 'created', 'permissions', 'contributor', 'hierarchy'])
    for attr in not_allowed_attributes:
        if attr in resource:
            raise ObjModelException(attr + " attribute cannot be setted through this api", http_response_code=403)

    standalone = ('target' not in resource) and ('source' not in resource)
    if not standalone:
        if 'target' in resource and 'source' in resource:
            raise ObjModelException('target and source are mutually exclusive', 403)

        link_key, referred_ids = get_referred_ids(resource)

        if not link_key:
            raise ObjModelException('resource cannot be orphan', 403)

        _validate_resource_linking(
            colln, acl_svc, link_key, referred_ids, user_id, team_ids, allow_blank_ids=allow_blank_ids,
            validate_permissions=validate_permissions, graph_cache=graph_cache
        )


# noinspection PyProtectedMember
def validate_update(
        colln, acl_svc, update_json, user_id, user_team_ids,
        not_allowed_attributes=None, allow_blank_ids=False,
        validate_permissions=True, graph_cache=None):

    old_resource = colln.find_one(resource_selector_doc(update_json['_id']))
    if old_resource is None:
        raise ObjModelException('resource does not exists', 403)

    not_allowed_attributes = list(not_allowed_attributes or [])
    not_allowed_attributes.extend(['creator', 'created', 'contributor', 'jsonClass', 'hierarchy'])
    for attr in not_allowed_attributes:
        if attr in update_json:
            if attr in old_resource and json.dumps(old_resource[attr]) == json.dumps(update_json[attr]):
                continue
            raise ObjModelException(attr + " attribute cannot be setted through this api", 403)

    links_updated = _validate_links_update(
        colln, acl_svc, old_resource, update_json, user_id, user_team_ids,
        allow_blank_ids=allow_blank_ids, validate_permissions=validate_permissions, graph_cache=graph_cache)

    # print(1, graph_cache)
    has_perm = PermissionResolver.resolve_permission(
        old_resource, PermissionResolver.UPDATE_CONTENT, user_id, user_team_ids,
        colln, acl_svc, resource_graph_cache=graph_cache) # To fill cache though, even when not validate_permissions
    # print(2, graph_cache)

    if validate_permissions and not has_perm:
        raise ObjModelException('permission denied', 403)
    return {"links_updated": links_updated}


def _validate_links_update(
        colln, acl_svc, old_resource, update_json, user_id, user_team_ids,
        allow_blank_ids=False, validate_permissions=True, graph_cache=None):

    if not ('target' in update_json or 'source' in update_json):
        return
    if 'target' in update_json and 'source' in update_json:
        raise ObjModelException('target and source fields are mutually exclusive', 403)

    update_link_key, update_referred_resource_ids = get_referred_ids(update_json)
    old_resource_link_key, old_referred_resource_ids = get_referred_ids(old_resource)

    '''
    if not old_resource_linking_key:
        raise ObjModelException('up-linking not allowed for this object')
    '''

    if old_resource_link_key:
        if old_resource_link_key != update_link_key:
            raise ObjModelException('link key cannot be changed', 403)

        '''
        if type(old_resource_link_attr) != type(update_link_attr):
            raise ObjModelException('cannot change type of link attr', 403)
        '''

        new_referred_object_ids = set(update_referred_resource_ids).difference(set(old_referred_resource_ids))
    else:
        new_referred_object_ids = set(update_referred_resource_ids)

    if not len(new_referred_object_ids):
        return False

    _validate_resource_linking(
        colln, acl_svc, update_link_key, new_referred_object_ids,
        user_id, user_team_ids, allow_blank_ids=allow_blank_ids, validate_permissions=validate_permissions, graph_cache=graph_cache)
    return True


def _validate_resource_linking(
        colln, acl_svc, referring_key, referred_object_ids,
        user_id, user_team_ids, allow_blank_ids=False, validate_permissions=True, graph_cache=None):

    for referred_resource_id in referred_object_ids:
        if referred_resource_id is None:
            continue

        if is_blank_id(referred_resource_id):
            if allow_blank_ids:
                continue
            else:
                raise ObjModelException('blank ids not allowed outside graph', 403)

        referred_resource = colln.find_one(
            resource_selector_doc(referred_resource_id),
            projection={"jsonClass": 1, "_id": 1, "target": 1, "source": 1, "hierarchy": 1})  # TODO no projection should be applied.

        if not referred_resource:
            raise ObjModelException('resource with _id "{}" does not exist'.format(referred_resource_id), 403)

        required_permission = (
            PermissionResolver.CREATE_CHILDREN if referring_key == 'source'
            else PermissionResolver.CREATE_ANNOS
        )

        has_perm = PermissionResolver.resolve_permission(
            referred_resource, required_permission,
            user_id, user_team_ids, colln, acl_svc, resource_graph_cache=graph_cache) # TODO user_id == None?

        if validate_permissions and user_id and not has_perm:
            raise ObjModelException(
                'permission denied to link with resource {}'.format(referred_resource_id), 403)


def compute_hierarchy(res, graph_cache):
    hierarchy = []
    link_key, referred_ids = get_referred_ids(res)
    if not link_key:
        return hierarchy
    for i, rid in enumerate(referred_ids):
        ref_hierarchy = [] + graph_cache[rid].get('hierarchy', [])
        ref = {'i': rid, 'c': graph_cache[rid]['jsonClass'], 'p': [i], 'e': 1}
        hierarchy.append(ref)
        for hitem in ref_hierarchy or []:
            hitem = deepcopy(hitem)
            hitem['p'] = [i] + hitem['p']
            hitem['e'] = len(hitem['p'])
            hierarchy.append(hitem)
    return hierarchy


def get_updated_referrer_hierarchy(referrer, p_hierarchy, p_id):
    rh = referrer['hierarchy']  # TODO forcing it to crash if not existed
    # print(referrer['_id'], rh)
    h_start_index = None

    def arr_starts_with(a1, a2):
        if len(a1) < len(a2):
            return False
        for i in range(len(a2)):
            if a1[i] != a2[i]:
                return False
        return True

    for i, hitem in enumerate(rh):
        if hitem.get('i') == p_id:
            h_start_index = i
            break
    # print(h_start_index)
    if h_start_index is None:
        return None # TODO should crash

    h_path = rh[h_start_index]['p']
    h_end_index = h_start_index + 1
    while h_end_index < len(rh):
        p = rh[h_end_index]['p']
        if not arr_starts_with(p, h_path):
            break
        h_end_index += 1

    p_h = []
    for hitem in p_hierarchy:
        hitem = hitem.copy()
        hitem['p'] = h_path + hitem['p']
        hitem['e'] = len(hitem['p'])
        p_h.append(hitem)

    return rh[:h_start_index + 1] + p_h + rh[h_end_index:]


def save_hierarchy_updates(colln, h_updates):
    #  print('saving hierarchies', len(h_updates))
    if len(h_updates):
        colln.bulk_update(h_updates, ordered=False)


def update_referrer_hierarchies(colln, res_id, res_h):
    referrers = colln.find_and_do({"hierarchy.i": res_id}, {}, return_cursor=True, projection={"_id": 1, "hierarchy": 1})
    h_updates = []

    for ref in referrers:
        rh = get_updated_referrer_hierarchy(ref, res_h, res_id)
        #  print(rh, '\n')
        if rh is None:
            continue
        h_updates.append((str(ref['_id']), {'$set': {'hierarchy': rh}}))
        if len(h_updates) == 5000:
            save_hierarchy_updates(colln, h_updates)
            h_updates.clear()

    if len(h_updates):
        save_hierarchy_updates(colln, h_updates)


# noinspection PyProtectedMember
def create_resource(
        colln, acl_svc, resource_json,
        user_id, user_team_ids, schema_validator,
        not_allowed_attributes=None, initial_agents=None, allow_blank_ids=False,
        validate_schema=True, validate_permissions=True, graph_cache=None):

    graph_cache = graph_cache if graph_cache is not None else {}

    validate_creation(
        colln, acl_svc, resource_json, user_id, user_team_ids,
        not_allowed_attributes=not_allowed_attributes, allow_blank_ids=allow_blank_ids,
        validate_permissions=validate_permissions, graph_cache=graph_cache
    )
    resource_json['hierarchy'] = compute_hierarchy(resource_json, graph_cache)
    json_object_helper.update_time(resource_json)

    if user_id:
        resource_json['creator'] = user_id
        resource_json['contributor'] = [user_id]
    if schema_validator:
        json_object_helper.set_type_field_recursively_for_doc(resource_json, schema_validator.vv_schemas)

    if validate_schema:
        schema_validator.validate(resource_json)
    inserted_id = colln.insert_one(resource_json).inserted_id

    acl = get_default_acl(user_id, initial_agents)
    acl['_id'] = inserted_id
    acl_svc.update(inserted_id, acl, upsert=True)  # NOTE

    return inserted_id


# noinspection PyProtectedMember
def update_resource(
        colln, acl_svc, update_json, user_id, user_team_ids, schema_validator: SchemaValidator,
        not_allowed_attributes=None, allow_blank_ids=False,
        validate_schema=True, validate_permissions=True, graph_cache=None):

    if validate_schema and schema_validator:
        schema_validator.validate(update_json, diff=True)

    graph_cache = graph_cache if graph_cache is not None else {}

    report = validate_update(
        colln, acl_svc, update_json, user_id, user_team_ids,
        not_allowed_attributes=not_allowed_attributes, allow_blank_ids=allow_blank_ids,
        validate_permissions=validate_permissions, graph_cache=graph_cache)

    # print(graph_cache.keys())
    if report.get('links_updated', None):
        update_json['hierarchy'] = compute_hierarchy(update_json, graph_cache)

    json_object_helper.update_time(update_json, just_update=True)
    update_json['contributor'] = [user_id]
    update_doc = {
        "$set": update_json,
        # "$addToSet": {
        #     "contributor": user_id
        # }  # TODO
    }
    response = colln.update_one(
        {"_id": update_json['_id'], 'jsonClass': update_json['jsonClass']}, update_doc)
    if not response.matched_count:
        return None
    if report.get('links_updated', None):
        #  print('updating ref hierarchy', update_json['_id'], update_json['hierarchy'], '\n\n')
        update_referrer_hierarchies(colln, update_json['_id'], update_json['hierarchy'])
    return update_json['_id']


def get_matched_resource_id_for_primary_key(
        resources_colln, resource_json, vv_schemas, blank_referred_ids_to_resolved_ids_map=None, skip_if_blank_links=False):
    json_class = resource_json.get('jsonClass', None)
    if not json_class:
        raise ObjModelException('jsonClass is required for action', http_response_code=400)

    vv_schema = vv_schemas.get(json_class)
    if not vv_schema:
        raise ObjModelException('unknown json_class {}'.format(json_class), 400)

    schema = vv_schema
    primary_key_sets = schema.get('_primary_keys', None)

    if not primary_key_sets:
        return None

    can_check_be_skipped = False  # TODO
    if skip_if_blank_links:
        links = _get_normalized_nested_links(resource_json, 'target') + _get_normalized_nested_links(resource_json, 'source')
        blank_links = [
            link for link in links
            if is_blank_id(link)
               and not (blank_referred_ids_to_resolved_ids_map and link in blank_referred_ids_to_resolved_ids_map)
        ]
        if blank_links:
            can_check_be_skipped = False not in ['source' in ks or 'target' in ks for ks in primary_key_sets]

    if can_check_be_skipped:
        # print('can be ignored', resource_json, blank_referred_ids_to_resolved_ids_map)
        return None

    primary_key_sets_query_doc = projection_helper.get_primary_key_query_doc(
        resource_json, primary_key_sets, blank_referred_ids_to_resolved_ids_map=blank_referred_ids_to_resolved_ids_map
    )
    #  print(resource_json['jsonClass'], primary_key_sets_query_doc)
    if not primary_key_sets_query_doc:
        return None
    matched_resource_json = resources_colln.find_one(primary_key_sets_query_doc, projection={"_id": 1})
    if not matched_resource_json:
        return None

    return matched_resource_json['_id']


def create_or_update(
        colln, acl_svc, resource_json, user_id, user_team_ids, schema_validator: SchemaValidator,
        upsert=False, non_updatable_attributes=None,
        initial_agents=None, allow_blank_ids=False, validate_schema=True, validate_permissions=True):

    if schema_validator:
        json_object_helper.set_type_field_recursively_for_doc(resource_json, schema_validator.vv_schemas)

    if "_id" not in resource_json:
        matched_resource_id_for_primary_key = get_matched_resource_id_for_primary_key(
            colln, resource_json, schema_validator.vv_schemas)
        if matched_resource_id_for_primary_key:
            if upsert:
                resource_json = resource_json.copy()
                resource_json['_id'] = matched_resource_id_for_primary_key
            else:
                raise ObjModelException(
                    'resource with matched primary_key already exist with _id {}'.format(
                        matched_resource_id_for_primary_key), 403
                )

    if '_id' in resource_json:
        return update_resource(
            colln, acl_svc, resource_json, user_id, user_team_ids, schema_validator,
            not_allowed_attributes=non_updatable_attributes, allow_blank_ids=allow_blank_ids,
            validate_schema=validate_schema, validate_permissions=validate_permissions)
    else:
        return create_resource(
            colln, acl_svc, resource_json, user_id, user_team_ids, schema_validator,
            initial_agents=initial_agents, allow_blank_ids=allow_blank_ids,
            validate_schema=validate_schema, validate_permissions=validate_permissions
        )


def _get_normalized_nested_links(doc, nested_field):
    try:
        v = projection_helper.get_nested_value(doc, nested_field) or []
    except ObjModelException:
        v = []
    if not isinstance(v, (str, list)):
        return None
    return {
        str: lambda attr: [attr],
        list: lambda attr: attr,
    }[type(v)](v)


def distribute_reached_ids(parent_ids, reached_nodes, keys):
    node_id_reached_ids_map = {}
    for rn in reached_nodes:
        for k in keys:
            links = _get_normalized_nested_links(rn, k)
            if links is None:
                continue
            for link in links:
                if link not in parent_ids:
                    continue
                if link not in node_id_reached_ids_map:
                    node_id_reached_ids_map[link] = {}
                reached_ids_map = node_id_reached_ids_map[link]
                if k not in reached_ids_map:
                    reached_ids_map[k] = []

                reached_ids_map[k].append(rn['_id'])
    return node_id_reached_ids_map


# noinspection PyProtectedMember
def get_default_acl(creator_id, initial_agents):
    acl = AclsGenerator.get_acl_template()

    if creator_id:
        AclsGenerator.add_to_agent_ids(
            acl, PermissionResolver.ACTIONS, PermissionResolver.GRANT, user_ids=[creator_id])

    if not initial_agents:
        return acl

    if hasattr(initial_agents, 'root_admins_team_id'):
        AclsGenerator.add_to_agent_ids(
            acl, PermissionResolver.ACTIONS, PermissionResolver.GRANT, team_ids=[initial_agents.root_admins_team_id])

    '''
    if hasattr(initial_agents, 'all_users_team_id'):
        PermissionsGenerator.add_to_agent_sets(
            permissions, [PermissionResolver.READ], 'granted', team_pids=[initial_agents.all_users_team_id])
    '''
    return acl


def add_to_agent_ids(
        colln, acl_svc, resource_id, current_user_id, current_team_ids, actions, control,
        get_user_fn, get_team_fn, user_ids=None, team_ids=None):

    if control not in PermissionResolver.CONTROLS:
        raise ObjModelException('invalid control')

    for action in actions:
        if action not in PermissionResolver.ACTIONS:
            raise ObjModelException('invalid action {}'.format(action), 403)

    if not len(actions):
        raise ObjModelException('invalid actions', 403)

    resource = colln.find_one(
        resource_selector_doc(resource_id), projection={"_id": 1, "target": 1, "source": 1})
    if resource is None:
        raise ObjModelException('resource not found', 404)

    acls_cache = {}
    if not PermissionResolver.resolve_permission(
            resource, PermissionResolver.UPDATE_PERMISSIONS,
            current_user_id, current_team_ids, colln, acl_svc, acls_cache=acls_cache):
        raise ObjModelException('permission denied', 403)

    if not acls_cache.get(resource_id):
        acl = get_default_acl(None, None)
        acl['_id'] = resource_id
        acl_svc.colln.insert_one(acl)

    user_ids = user_ids or []
    team_ids = team_ids or []

    for user_id in user_ids:
        if user_id is None or user_id == '*':
            continue
        user = get_user_fn(user_id, projection={"_id": 1, "jsonClass": 1})
        if user is None:
            raise ObjModelException('user {} does not exist'.format(user_id), 403)
    for team_id in team_ids:
        if team_id == '*':
            continue
        team = get_team_fn(team_id, projection={"_id": 1, "jsonClass": 1})
        if team is None:
            raise ObjModelException('team {} does not exist'.format(team_id), 403)

    response = acl_svc.add_to_agent_ids(
        [resource_id], actions, control, user_ids=user_ids, team_ids=team_ids)
    return response.modified_count


def remove_from_permissions_agent_set(
        colln, acl_svc, resource_id, current_user_id, current_team_ids, actions, control,
        user_ids=None, team_ids=None):

    if control not in PermissionResolver.CONTROLS:
        raise ObjModelException('invalid control')

    for action in actions:
        if action not in PermissionResolver.ACTIONS:
            raise ObjModelException('invalid action {}'.format(action), 403)

    if not len(actions):
        raise ObjModelException('invalid actions', 403)

    resource = colln.find_one(
        resource_selector_doc(resource_id), projection={"_id": 1, "target": 1, "source": 1})
    if resource is None:
        raise ObjModelException('resource not found', 404)
    if not PermissionResolver.resolve_permission(
            resource, PermissionResolver.UPDATE_PERMISSIONS,
            current_user_id, current_team_ids, colln, acl_svc):
        raise ObjModelException('permission denied', 403)

    response = acl_svc.remove_from_agent_ids(
        [resource_id], actions, control, user_ids=user_ids, team_ids=team_ids)
    return response.modified_count


def delete_tree_new(
        colln, acl_svc, data_dir_path, root_nodes,
        user_id, team_ids, cumulative_traversed_ids=None, deleted_ids=None, graph_cache=None, acls_cache=None
        ):
    graph_cache = graph_cache if graph_cache is not None else {}
    acls_cache = acls_cache if acls_cache is not None else {}
    cumulative_traversed_ids = cumulative_traversed_ids or set()
    un_crawled_ids = []
    deleted_ids = deleted_ids or []

    root_node_ids = []
    node_id_children_map = {}

    for i, root_node in enumerate(root_nodes):
        root_node_id = root_node['_id']
        root_node_ids.append(root_node_id)
        #  if root_node_id not in cumulative_traversed_ids:
        un_crawled_ids.append(root_node_id)
        node_id_children_map[root_node_id] = set()  # can be dict keyed by keys.

        cumulative_traversed_ids.add(root_node_id)
        graph_cache[root_node_id] = root_node

    if not len(un_crawled_ids):
        return {}, deleted_ids

    children_selector_doc = {
        "$or": [
            {"source": {"$in": un_crawled_ids}},
            {"target": {"$in": un_crawled_ids}}
        ]
    }
    children_projection = {"jsonClass": 1, "_id": 1, "source": 1, "target": 1}

    children = colln.find(
        children_selector_doc, projection=children_projection)

    for child in children:
        _, links = get_referred_ids(child)
        for link in links:
            if link in node_id_children_map:
                node_id_children_map[link].add(child['_id'])

    # noinspection PyNoneFunctionAssignment,PyTupleAssignmentBalance
    children_delete_approvals, deleted_ids = delete_tree_new(
        colln, acl_svc, data_dir_path, children,
        user_id, team_ids, cumulative_traversed_ids=cumulative_traversed_ids,
        graph_cache=graph_cache, acls_cache=acls_cache, deleted_ids=deleted_ids)

    delete_approvals = {}
    for root_node in root_nodes:
        root_node_id = root_node['_id']
        children_ids = node_id_children_map.get(root_node_id, [])
        children_approval = False not in [children_delete_approvals[child_id] for child_id in children_ids]
        delete_approvals[root_node_id] = children_approval

    deletable_node_ids = [node_id for node_id in delete_approvals if delete_approvals.get(node_id)]
    resolved_permissions_list = PermissionResolver.get_resolved_permissions_for_resources(
        colln, acl_svc, [graph_cache[node_id] for node_id in deletable_node_ids],
        PermissionResolver.ACTIONS, user_id, team_ids,
        resource_graph_cache=graph_cache, acls_cache=acls_cache)

    for i, root_node_id in enumerate(deletable_node_ids):
        resolved_permissions = resolved_permissions_list[i]
        if resolved_permissions is None:
            continue

        if not resolved_permissions.get(PermissionResolver.DELETE):
            delete_approvals[root_node_id] = False
            continue
        if data_dir_path:
            file_repr_path = os.path.join(data_dir_path, root_node_id)
            if os.path.exists(file_repr_path):
                os.remove(file_repr_path)

    deletable_node_ids = [node_id for node_id in delete_approvals if delete_approvals.get(node_id)]

    print('deleting {} resources'.format(len(deletable_node_ids)))
    colln.delete_many({"_id": {"$in": deletable_node_ids}})
    acl_svc.delete(deletable_node_ids)
    deleted_ids.extend(deletable_node_ids)
    return delete_approvals, deleted_ids


def delete_tree(
        colln, acl_svc, data_dir_path, root_node_or_id,
        user_id, user_team_ids, permission_resolver=PermissionResolver):

    if isinstance(root_node_or_id, dict):
        root_node = root_node_or_id  # type: dict
        # noinspection PyUnresolvedReferences,PyProtectedMember
        root_node_id = root_node_or_id['_id']
    else:
        root_node = colln.find_one(
            resource_selector_doc(root_node_or_id),
            projection={"jsonClass": 1, "_id": 1, "source": 1, "target": 1})
        if root_node is None:
            return False, []
        root_node_id = root_node_or_id

    if permission_resolver is not None:
        has_delete_permission = permission_resolver.resolve_permission(
            root_node, PermissionResolver.DELETE, user_id, user_team_ids, colln, acl_svc)
        if not has_delete_permission:
            return False, []

    referrers = colln.find(
        {"$or": [{"target": root_node_id}, {"source": root_node_id}]},
        projection={"jsonClass": 1, "_id": 1, "source": 1, "target": 1})

    descendant_approvals = []
    deleted_res_ids = []

    for r in referrers:
        approval, deleted_descendant_ids = delete_tree(
            colln, acl_svc, data_dir_path, r, user_id, user_team_ids, permission_resolver=permission_resolver)
        descendant_approvals.append(approval)
        deleted_res_ids.extend(deleted_descendant_ids)

    if False not in descendant_approvals:
        colln.delete_item(root_node_id)
        acl_svc.delete([root_node_id])

        if data_dir_path:
            file_repr_path = os.path.join(data_dir_path, root_node_id)
            if os.path.exists(file_repr_path):
                os.remove(file_repr_path)

        deleted_res_ids.append(root_node_id)
        return True, deleted_res_ids

    return False, deleted_res_ids


def delete_selection(
        colln, acl_svc, data_dir_path, selector_doc, user_id, team_ids):
    matched_docs = colln.find(query=selector_doc, projection={'_id': 1})

    delete_status, all_deleted_ids = delete_tree_new(colln, acl_svc, data_dir_path, matched_docs, user_id, team_ids)

    return delete_status, all_deleted_ids
