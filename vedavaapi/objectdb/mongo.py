import pymongo
# noinspection PyPackageRequirements
import bson
# noinspection PyPackageRequirements
from bson import ObjectId
from pymongo import InsertOne, UpdateOne, UpdateMany, ReplaceOne, DeleteMany, DeleteOne
from pymongo.cursor import Cursor

from vedavaapi.objectdb import DotDict
from .mydb import *


# noinspection PyProtectedMember
class MongoDbCollection(DBMSCollection):

    operations = DotDict({
        'InsertOne': InsertOne,
        'UpdateOne': UpdateOne,
        'Updatemany': UpdateMany,
        'ReplaceOne': ReplaceOne,
        'DeleteOne': DeleteOne,
        'DeleteMany': DeleteMany
    })

    def __init__(self, mongo_database, collection_name):
        super(MongoDbCollection, self).__init__(mongo_database, collection_name)
        self._mongo_collection = self.dbms_db._mongo_collection(collection_name)  # type: pymongo.collection.Collection

    def all(self):
        return self._mongo_collection.find()

    def count(self, filter_doc, **kwargs):
        return self._mongo_collection.count_documents(filter_doc, **kwargs)

    def drop(self):
        return self._mongo_collection.drop()
        
    def find_one(self, query, projection=None):
        result = self._mongo_collection.find_one(query, projection=projection)
        return result

    def find(self, query, projection=None, cursor=False):
        return self._mongo_collection.find(query, projection)  # type: Cursor

    def insert_one(self, item):
        mongo_response = self._mongo_collection.insert_one(item)
        response = DotDict({
            'inserted_id': self.string_id(mongo_response.inserted_id)
        })
        return response

    def insert_many(self, items):
        mongo_response = self._mongo_collection.insert_many(items)
        response = DotDict({
            'inserted_ids': [self.string_id(inserted_id) for inserted_id in mongo_response.inserted_ids]
        })
        return response
        
    def update_one(self, query, update_doc, upsert=False):
        mongo_response = self._mongo_collection.update_one(query, update_doc, upsert=upsert)
        response = DotDict({
            'modified_count': mongo_response.modified_count,
            'matched_count': mongo_response.matched_count
        })
        return response

    def update_many(self, query, update_doc, upsert=False):
        mongo_response = self._mongo_collection.update_many(query, update_doc, upsert=upsert)
        response = DotDict({
            'modified_count': mongo_response.modified_count,
            'matched_count': mongo_response.matched_count
        })
        return response

    def delete_one(self, query):
        mongo_response = self._mongo_collection.delete_one(query)
        response = DotDict({
            'deleted_count': mongo_response.deleted_count
        })
        return response

    def delete_many(self, query):
        mongo_response = self._mongo_collection.delete_many(query)
        response = DotDict({
            'deleted_count': mongo_response.deleted_count
        })
        return response

    def bulk_write(self, requests, ordered=True, bypass_document_validation=False, session=None):
        mongo_response = self._mongo_collection.bulk_write(
            requests, ordered=ordered, bypass_document_validation=bypass_document_validation, session=session)
        response = DotDict({
            'bulk_api_result': mongo_response.bulk_api_result,
            'deleted_count': mongo_response.deleted_count,
            'inserted_count': mongo_response.inserted_count,
            'matched_count': mongo_response.matched_count,
            'modified_count': mongo_response.modified_count,
            'upserted_count': mongo_response.modified_count,
            'upserted_ids': [self.string_id(inserted_id) for inserted_id in mongo_response.upserted_ids]
        })
        return response

    def find_one_and_update(self, query, update_doc, upsert=False, return_doc='after'):
        mongo_return_doc = {
            'before': pymongo.collection.ReturnDocument.BEFORE,
            'after': pymongo.collection.ReturnDocument.AFTER
        }.get(return_doc, pymongo.collection.ReturnDocument.AFTER)
        doc = self._mongo_collection.find_one_and_update(
            query,
            update_doc,
            upsert=upsert,
            return_document=mongo_return_doc
        )
        return doc

    def object_id(self, _id):
        try:
            return ObjectId(_id) if isinstance(_id, six.string_types) else _id
        except bson.errors.InvalidId:
            return _id

    def string_id(self, _id):
        return _id if isinstance(_id, six.string_types) else str(_id)

    def create_index(self, keys_dict, index_name):
        self._mongo_collection.create_index(list(keys_dict.items()), name=index_name, background=True)


# noinspection PyProtectedMember
class MongoDbDatabase(DBMSDatabase):

    def __init__(self, mongo_client, db_name):
        super(MongoDbDatabase, self).__init__(mongo_client, db_name)
        self._mongo_database = self.dbms_client._mongo_database(db_name)

    def _mongo_collection(self, collection_name):
        return self._mongo_database.get_collection(collection_name)

    def list_collection_names(self):
        return self._mongo_database.list_collection_names()

    def get_collection(self, collection_name):
        return MongoDbCollection(self, collection_name)

    def drop_collection(self, collection_name):
        self._mongo_database.drop_collection()


class MongoDbClient(DBMSClient):

    def __init__(self, host_uri, port=None):
        port = 27017 if port is None else port
        super(MongoDbClient, self).__init__(host_uri, port=port)
        self._mongo_client = pymongo.MongoClient(host_uri, port=port)

    def _mongo_database(self, db_name):
        return self._mongo_client.get_database(db_name)

    def get_database(self, db_name):
        return MongoDbDatabase(self, db_name)

    def list_database_names(self):
        return self._mongo_client.list_database_names()

    def drop_database(self, db_name):
        return self._mongo_client.drop_database(db_name)

    def close(self):
        self._mongo_client.close()
